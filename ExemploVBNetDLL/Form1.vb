﻿Public Class Form1
    Dim p As IntPtr
    Dim i As Integer
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        'Dim p As IntPtr
        'Dim i As Integer

        p = New IntPtr

        i = FuncoesDll.PrinterCreator(p, "i7")
        i = FuncoesDll.PortOpen(p, "USB")

        If i = 0 Then
            Label1.Text = "Status: Impressora Pronta!"
        Else
            Label1.Text = "Status: Algo deu errado. Favor Verificar!"
        End If


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim texto As String

        i = FuncoesDll.PrinterInitialize(p)
        i = FuncoesDll.SetTextFont(p, 1)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "Elgin Manaus"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "Elgin S/A"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "Rua Abiurana, 579 Distrito Industrial Manaus - AM"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)
        texto = "CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "EXTRATO No. 002046"
        i = FuncoesDll.PrintText(p, texto, 1, 2, 0)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "CUPOM FISCAL ELETRONICO - SAT"
        i = FuncoesDll.PrintText(p, texto, 1, 2, 0)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.PrinterInitialize(p)
        i = FuncoesDll.SetTextFont(p, 1)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 0)
        texto = "CPF/CNPJ consumidor:"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "# | COD | DESC | QTD | UN | VL UN R$ | (VL TR r$)* | VL ITEM r$"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "| 1000016  VRAAAAAAAAAAAAAAAAAAAAAAU 3,449 L 2,9++ (3.84)  10.00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)
        texto = "Subtotal                                                   10,00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "TOTAL R$                                                   10,00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)
        texto = "Dinheiro                                                   10,00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "Troco R$                                                    0,00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "Tributos Totais (Lei Fed 12.741/12) R$                      3,85"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 0)
        texto = "R$ 3,84 Trib aprox R$ 1,35 Fed R$ 2,50 Est"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "Fonte: IBPT/FECOMERCIO (RS) 9oi3aC"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)
        texto = "KM: 0"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 0)
        texto = "* Valor aproximado dos tributos do item"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "----------------------------------------------------------------"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "SAT No. 900001231"
        i = FuncoesDll.PrintText(p, texto, 1, 2, 0)
        i = FuncoesDll.FeedLine(p, 0)
        texto = "01/01/2018 - 00:00:00"
        i = FuncoesDll.PrintTextS(p, texto)
        i = FuncoesDll.FeedLine(p, 1)

        ' CODE128 - CÓDIGO DE BARRAS

        i = FuncoesDll.PrinterInitialize(p)
        i = FuncoesDll.SetTextFont(p, 1)
        i = FuncoesDll.SetAlign(p, 1)
        texto = "{B35150661099008000141593515066109900800014159"
        i = FuncoesDll.PrintBarCode(p, 73, texto, 1, 60, 1, 0)
        i = FuncoesDll.FeedLine(p, 6)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim status As Integer
        status = New Integer

        i = FuncoesDll.GetPrinterState(p, status)

        If status = 0 Then
            Label3.Text = "IMPRESSORA PRONTA"
        End If
        If status = 2 Then
            Label3.Text = "TAMPA DA IMPRESSORA ABERTA"
        End If
        If status = 128 Then
            Label3.Text = "IMPRESSORA INATIVA. VERIFICAR"
        End If

    End Sub
End Class
