﻿Imports System.Runtime.InteropServices

Public Class FuncoesDll

    <DllImport("ESC_SDK.dll", EntryPoint:="PrinterCreator", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PrinterCreator(ByRef handle As IntPtr, <[In](), MarshalAs(UnmanagedType.LPTStr)> ByVal model As String) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="PortOpen", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PortOpen(ByVal handle As IntPtr, <[In](), MarshalAs(UnmanagedType.LPTStr)> ByVal ioSettings As String) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="PrinterInitialize", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PrinterInitialize(ByVal handle As IntPtr) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="SetTextFont", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function SetTextFont(ByVal handle As IntPtr, ByVal font As Integer) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="SetAlign", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function SetAlign(ByVal handle As IntPtr, ByVal align As Integer) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="PrintTextS", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PrintTextS(ByVal handle As IntPtr, <[In](), MarshalAs(UnmanagedType.LPTStr)> ByVal data As String) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="FeedLine", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function FeedLine(ByVal handle As IntPtr, ByVal lines As Integer) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="PrintText", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PrintText(ByVal handle As IntPtr, ByVal data As String, ByVal alignment As Integer, ByVal attribute As Integer, ByVal textSize As Integer) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="PrintBarCode", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function PrintBarCode(ByVal handle As IntPtr, ByVal bcType As Integer, ByVal bcData As String, ByVal width As Integer, ByVal height As Integer, ByVal alignment As Integer, ByVal hriPosition As Integer) As Integer
    End Function

    <DllImport("ESC_SDK.dll", EntryPoint:="GetPrinterState", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Unicode)>
    Public Shared Function GetPrinterState(ByVal handle As IntPtr, ByRef printerStatus As Integer) As Integer
    End Function

End Class
